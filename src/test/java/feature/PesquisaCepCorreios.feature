#language: pt
#encoding: iso-8859-1
#author: Bruno Carneiro (bruno_graciano@outlook.com)

Funcionalidade: Pesquisa CEP no site dos Correios

  Contexto: Realizar busca de CEP

  @buscaporcep
  Cen�rio: CT1 Realizar a busca de 1 CEP no site dos correios
    Dado que eu estou no site dos correios
    E realizo uma busca de CEP
      | Busca Cep |
      |  18050600 |
    Ent�o o resultado � exibido

  @buscalogradouro
  Cen�rio: CT2 Realizar a busca de logradouro no site dos correios
    Dado que eu estou no site dos correios
    E realizo uma busca de CEP
      | Busca Cep         |
      | Alameda Tocantins |
    Ent�o o resultado � exibido
