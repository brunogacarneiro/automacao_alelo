package steps;

import org.junit.After;
import org.junit.Before;

import cucumber.api.Scenario;
import utils.StringUtils;

public class Hooks {
	@Before
	public void beforeScenario(Scenario scenario) {
		System.out.println("Iniciando a execu��o do teste: " + scenario.getName());
		Thread.currentThread().setName(StringUtils.removeAcentuacao(scenario.getName()));
	}

	@After
	public void afterScenario(Scenario scenario) {
		System.out.println(
				"\nFinalizando a execu��o do teste: " + scenario.getName() + " com o status: " + scenario.getStatus());
		boolean passed = false;
		if (scenario.getStatus().equals("passed")) {
			passed = true;
		}

	}

}
