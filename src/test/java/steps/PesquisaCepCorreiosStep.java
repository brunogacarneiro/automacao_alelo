package steps;

import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Ent�o;
import page.actions.CorreiosActions;
import page.actions.ResultadoCorreiosAction;

public class PesquisaCepCorreiosStep {
	
	CorreiosActions correiosaction_ = new CorreiosActions();

	@Dado("^que eu estou no site dos correios$")
	public void queEuEstouNoSiteDosCorreios() throws Throwable {		
		correiosaction_.navegarSite();
	}

	@E("^realizo uma busca de CEP$")
	public void realizoUmaBuscaDeCEP(DataTable data) throws Throwable {		
		for (Map<String, String> map : data.asMaps(String.class, String.class)) {			
			correiosaction_.preencherCampoBusca(map.get("Busca Cep"));
		}
		correiosaction_.clicarBuscar();
	}

	@Ent�o("^o resultado � exibido$")
	public void oResultado�Exibido() throws Throwable {		
		ResultadoCorreiosAction resultadosCorreiosAction = new ResultadoCorreiosAction();
		resultadosCorreiosAction.verificaSeProximaTabela();		
	}

}
