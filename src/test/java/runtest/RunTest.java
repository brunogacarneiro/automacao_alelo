package runtest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = false, 
				 snippets = SnippetType.CAMELCASE, 
				 features = {"./src/test/java/feature" }, 
				 glue = "steps", 
				 tags = {""})
public class RunTest {
	

}