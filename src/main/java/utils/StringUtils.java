package utils;

import java.text.Normalizer;

public class StringUtils {

	public static String removeAcentuacao(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		return texto.replaceAll("[^\\p{ASCII}]", "");
	}

}
