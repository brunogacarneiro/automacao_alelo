package page.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import page.objects.BasePage;
import page.objects.ResultadoCorreiosPage;

public class ResultadoCorreiosAction extends BasePage {

	ResultadoCorreiosPage page = new ResultadoCorreiosPage();
	List<Map<String, String>> resultado = new ArrayList<>();

	private void imprimeResultado() {
		for (Map<String, String> map : resultado) {
			System.out.println(map);
		}

	}

	public void leTabela() {
		Map<String, String> map;
		int rows = driver.findElements(By.xpath("//table/tbody/tr")).size();
		for (int j = 2; j <= rows; j++) {
			for (int i = 1; i <= 4; i++) {
				map = new HashMap<>();
				String titulo = obterTexto(By.xpath("//table/tbody/tr[1]/th[" + i + "]"));
				String valor = obterTexto(By.xpath("//table/tbody/tr[" + j + "]/td[" + i + "]"));
				map.put(titulo, valor);
				resultado.add(map);

			}
		}
	}

	public void verificaSeProximaTabela() {
		Object[] openedWindows = driver.getWindowHandles().toArray();
		driver.switchTo().window(openedWindows[1].toString());
		By botaoProximaPagina = By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[2]/div[2]/div[2]/div[5]/a");
		try {
			leTabela();
			while (!driver.findElement(botaoProximaPagina).getAttribute("href").isEmpty()) {
				scrollElementIntoView(botaoProximaPagina);
				clicarElemento(botaoProximaPagina);
				leTabela();

			}
		} catch (NoSuchElementException e) {

		} finally {
			imprimeResultado();
		}

	}

}
