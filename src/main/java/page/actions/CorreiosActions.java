package page.actions;

import page.objects.BasePage;
import page.objects.CorreiosPage;
import utils.PropertyReader;

public class CorreiosActions extends BasePage {

	CorreiosPage page = new CorreiosPage();

	public void navegarSite() {
		driver.get(PropertyReader.getProperty("automacao.web.url"));
	}

	public void preencherCampoBusca(String cep) {
		scrollElementIntoView(page.getCampoBuscarCep());
		preencherCampo(page.getCampoBuscarCep(), cep);
	}
	
	public void clicarBuscar() {
		clicarElemento(page.getBotaoBuscarCep());
	}

}
