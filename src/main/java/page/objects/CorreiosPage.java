package page.objects;

import org.openqa.selenium.By;

public class CorreiosPage {

	private final By campoBuscarCep = By.id("acesso-busca");
	private final By botaoBuscarCep = By.xpath("//*[@id='content-servicos']/ul/li[2]/form/div[2]/input[2]");

	public By getCampoBuscarCep() {
		return campoBuscarCep;
	}

	public By getBotaoBuscarCep() {
		return botaoBuscarCep;
	}
}
