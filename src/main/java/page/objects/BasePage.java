package page.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import webdriver.DriverFactory;

public class BasePage {

	protected WebDriver driver = DriverFactory.getDriver();

	private WebElement waitForElementToBeClickable(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = driver.findElement(by);
		wait.until(ExpectedConditions.elementToBeClickable(element)).click();
		return element;
	}

	protected void preencherCampo(By by, String texto) {
		WebElement element = waitForElementToBeClickable(by);
		element.clear();
		element.click();
		element.sendKeys(texto);
	}

	protected void scrollElementIntoView(By by) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(by));
	}

	protected void clicarElemento(By by) {
		waitForElementToBeClickable(by);
	}

	public String obterTexto(By by) {
		return driver.findElement(by).getText();
	}

	public void trocarAba() {
		driver.switchTo().window(driver.getWindowHandle());

	}

}
