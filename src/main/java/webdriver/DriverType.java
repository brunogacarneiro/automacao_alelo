package webdriver;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public enum DriverType implements DriverSetup {

	FIREFOX {
		public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions();
			options.merge(capabilities);
			options.setHeadless(HEADLESS);

			return new FirefoxDriver(options);
		}
	},
	CHROME {
		public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
			HashMap<String, Object> chromePreferences = new HashMap<>();
			ChromeOptions options = new ChromeOptions();
			chromePreferences.put("plugins.always_open_pdf_externally", true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			options.addArguments("--no-default-browser-check");
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", chromePreferences);
			options.merge(capabilities);
			return new ChromeDriver(options);
		}
	},
	IE {
		public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			System.setProperty("webdriver.ie.driver", "./src/test/resources/drivers/chromedriver.exe");
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.merge(capabilities);
			options.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			options.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

			return new InternetExplorerDriver(options);
		}

		@Override
		public String toString() {
			return "internet explorer";
		}
	},
	EDGE {
		public RemoteWebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			System.setProperty("webdriver.edge.driver", "./src/test/resources/drivers/msedgedriver.exe");
			EdgeOptions options = new EdgeOptions();
			options.merge(capabilities);

			return new EdgeDriver(options);
		}
	};

	public final static boolean HEADLESS = Boolean.getBoolean("headless");

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
	

}
