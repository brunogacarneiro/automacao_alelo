package webdriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.After;
import utils.PropertyReader;

public class DriverFactory {

	private static final String PROXY = null;
	private static final ProxyType MANUAL = null;
	private static WebDriver driver;
	private static DriverType selectedDriverType;

	private final static boolean useRemoteWebDriver = Boolean.getBoolean("remoteDriver");
	private final static boolean proxyEnabled = Boolean.getBoolean("proxyEnabled");
	private final static String proxyHostname = System.getProperty("proxyHost");
	private final static Integer proxyPort = Integer.getInteger("proxyPort");
	private final static String proxyDetails = String.format("%s:%d", proxyHostname, proxyPort);

	public DriverFactory() {
		DriverType driverType = DriverType.CHROME;
		try {
			driverType = DriverType.valueOf(getBrowser().toUpperCase());
		} catch (IllegalArgumentException ignored) {
			System.err.println("Driver desconhecido especificado, com o padr�o de '" + driverType + "'...");
		} catch (NullPointerException ignored) {
			System.err.println("Nenhum driver especificado, com o padr�o de '" + driverType + "'...");
		}
		selectedDriverType = driverType;
	}

	public static WebDriver getDriver() {
		if (null == driver) {
			try {
				instantiateWebDriver(DriverType.CHROME);
			} catch (MalformedURLException e) {
				throw new RuntimeException("N�o foi poss�vel obter a inst�ncia do WebDriver. " + e.getMessage());
			}
		}
		return driver;
	}

	public WebDriver getStoredDriver() {
		return driver;
	}

	
	public static void quitDriver() {
		if (null != driver) {
			driver.quit();
			driver = null;
		}
	}

	private static void instantiateWebDriver(DriverType driverType) throws MalformedURLException {

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

		if (proxyEnabled) {
			Proxy proxy = new Proxy();
			proxy.setProxyType(MANUAL);
			proxy.setHttpProxy(proxyDetails);
			proxy.setSslProxy(proxyDetails);
			desiredCapabilities.setCapability(PROXY, proxy);
		}

		if (useRemoteWebDriver) {
			URL seleniumGridURL = new URL(System.getProperty("gridURL"));
			String desiredBrowserVersion = System.getProperty("desiredBrowserVersion");
			String desiredPlatform = System.getProperty("desiredPlatform");

			if (null != desiredPlatform && !desiredPlatform.isEmpty()) {
				desiredCapabilities.setPlatform(Platform.valueOf(desiredPlatform.toUpperCase()));
			}

			if (null != desiredBrowserVersion && !desiredBrowserVersion.isEmpty()) {
				desiredCapabilities.setVersion(desiredBrowserVersion);
			}

			desiredCapabilities.setBrowserName(selectedDriverType.toString());
			driver = new RemoteWebDriver(seleniumGridURL, desiredCapabilities);
		} else {
			driver = driverType.getWebDriverObject(desiredCapabilities);
		}
	}

	private String getBrowser() {
		String browser = "";
		String browserVarAmbiente = System.getenv("browser");
		if (browserVarAmbiente == null) {
			browser = PropertyReader.getProperty("browser.default");
		}
		return browser;
	}
}
